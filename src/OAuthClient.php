<?php
namespace Sportily\OAuth;

interface OAuthClient {

    public function getToken($grant_type = 'client_credentials', $data = []);

    public function refreshToken($token);

    public function authorizeUrl();

    public function logoutUrl($redirect_url);

}
