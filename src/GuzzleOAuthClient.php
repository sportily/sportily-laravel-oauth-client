<?php
namespace Sportily\OAuth;

use Carbon\Carbon;
use GuzzleHttp\Client;

/**
 * An OAuth client that handles all requests to the OAuth server.
 */
class GuzzleOAuthClient implements OAuthClient {

    // The path to get the user to authorize the client.
    const AUTHORIZE_PATH = '/oauth/authorize';

    // The path to request access tokens.
    const TOKEN_PATH = '/oauth/token';

    // The path to logout.
    const LOGOUT_PATH = '/logout';

    // The base URL of the OAuth server.
    private $base_url;

    // The redirect URL the OAuth server should send the user back to.
    private $redirect_url;

    // The public key of the client.
    private $client_id;

    // The private/secret key of the client.
    private $client_secret;

    /**
     * Construct a new instance, using the user defined configuration.
     */
    public function __construct($config) {
        $this->base_url = $config['base_url'];
        $this->redirect_url = $config['redirect_url'];
        $this->client_id = $config['client_id'];
        $this->client_secret = $config['client_secret'];
    }

    /**
     * Request a new access token, defaulting to the client credentials grant
     * type. Supported types are:
     *  - client_credentials
     *  - authorization_code
     *  - refresh_token
     */
    public function getToken($grant_type = 'client_credentials', $data = []) {
        $body = array_merge([
            'grant_type' => $grant_type,
            'client_id' => $this->client_id,
            'client_secret' => $this->client_secret
        ], $data);

        // auth code requests require the redirect URL.
        if ($grant_type == 'authorization_code') {
            $body['redirect_uri'] = $this->redirect_url;
        }

        // actually make the request.
        $client = new Client(['base_url' => $this->base_url]);
        $request = $client->createRequest('POST', static::TOKEN_PATH, ['body' => $body]);
        $json = $client->send($request)->json();

        // build a Token instance from the response.
        $token = $json['access_token'];
        $expiry = Carbon::now()->addSeconds($json['expires_in']);
        $refresh_token = isset($json['refresh_token']) ? $json['refresh_token'] : null;
        return new Token($token, $expiry, $grant_type, $refresh_token);
    }

    /**
     * Request a new access token, using the refresh token associated with the
     * given (persumably expired) access token.
     */
    public function refreshToken($token) {
        return $this->getToken('refresh_token', [
            'refresh_token' => $token->refresh
        ]);
    }

    /**
     * Generate a URL to the authorize controller on the OAuth server.
     */
    public function authorizeUrl() {
        return $this->base_url . static::AUTHORIZE_PATH
            . '?response_type=code'
            . '&client_id=' . $this->client_id
            . '&redirect_uri=' . $this->redirect_url
            . '&return_to='. request()->getRequestUri();
    }

    /**
     * Generate a URL to the logout controller on the OAuth server.
     */
    public function logoutUrl($redirect_url) {
        return $this->base_url . static::LOGOUT_PATH
            . '?redirect=' . $redirect_url;
    }

}
