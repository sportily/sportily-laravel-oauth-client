<?php
namespace Sportily\OAuth\Middleware;

use Cache;
use Closure;
use Session;
use Sportily\OAuth\OAuthClient;
use Sportily\OAuth\Token;

/**
 * Laravel middleware to ensure an access token suitable for fetching public
 * data from the API is always available in the session.
 *
 * 1. Look for a valid token in the session.
 * 2. Refresh the token if possible.
 * 3. Look for a vaild token in the cache.
 * 4. Request a new token, using the client_credentials grant.
 * 5. Put the token back in the session.
 */
class OAuthMiddlewarePublic {

    // The OAuth client that handles requests to the OAuth service.
    private $oauth;

    /**
     * Construct a new middleware instance.
     */
    public function __construct(OAuthClient $oauth) {
        $this->oauth = $oauth;
    }

    /**
     * Handle the request.
     */
    public function handle($request, Closure $next) {
        // check the session for a token.
        $token = $this->getSessionToken();
        if (!$token->isValid()) {

            // attempt to refresh the token.
            if ($token->supportsRefresh()) {
                $token = $this->oauth->refreshToken($token);

            } else {
                // check the cache for a token.
                $token = $this->getCacheToken();
                if (!$token->isValid()) {

                    // generate a new token, and cache it.
                    $token = $this->oauth->getToken();
                    $this->putCacheToken($token);
                }
            }

            // store the token in the session.
            $this->putSessionToken($token);
        }

        return $next($request);
    }

    /**
     * Fetch the access token from the session, or generate an invalid token
     * if there is nothing in the session.
     */
    private function getSessionToken() {
        return Session::get('access_token', Token::invalid());
    }

    /**
     * Write an access token back into the session.
     */
    private function putSessionToken($token) {
        Session::put('access_token', $token);
    }

    /**
     * Fetch the access token from the cache, or generate an invalid token
     * if there is nothing in the cache.
     */
    private function getCacheToken() {
        return Cache::get('access_token', Token::invalid());
    }

    /**
     * Write an access token back into the cache.
     */
    private function putCacheToken($token) {
        Cache::forever('access_token', $token);
    }
}
