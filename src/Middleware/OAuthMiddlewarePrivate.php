<?php
namespace Sportily\OAuth\Middleware;

use Closure;
use GuzzleHttp\Exception\ClientException;
use Session;
use Request;
use Carbon\Carbon;
use Sportily\OAuth\OAuthClient;
use Sportily\OAuth\Token;


/**
 * Laravel middleware to ensure routes are protected from unauthorized access,
 * sending the user to the OAuth server to signin if not authenticated.
 */
class OAuthMiddlewarePrivate {

    private static $VALID_TYPES = ['authorization_code', 'refresh_token'];

    // The OAuth client that handles requests to the OAuth service.
    private $oauth;

    /**
     * Construct a new middleware instance.
     */
    public function __construct(OAuthClient $oauth) {
        $this->oauth = $oauth;
    }

    /**
     * Handle the request.
     */
    public function handle($request, Closure $next) {
        $token = $this->getSessionToken();

        // only valid 'auth code' tokens are permitted.
        if (!$this->tokenIsValid($token)) {

            // attempt to refresh the token if possible.
            if ($token->supportsRefresh()) {
                try {
                    $token = $this->oauth->refreshToken($token);
                    $this->putSessionToken($token);

                } catch (ClientException $e) {
                    // unable to refresh the token, so back to login.
                    return redirect($this->oauth->authorizeUrl() . '&' . http_build_query($request->all()));
                }

            } else {
                // send the user off the oauth server to login.
                return redirect($this->oauth->authorizeUrl() . '&' . http_build_query($request->all()));
            }
        }

        return $next($request);
    }

    private function tokenIsValid($token) {
        return $token->isValid()
            && in_array($token->type, static::$VALID_TYPES);
    }

    /**
     * Fetch the access token from the session, or generate an invalid token
     * if there is nothing in the session.
     */
     private function getSessionToken() {


         if(Request::get('access_token')) {
             $expiry = Carbon::createFromTimestamp(Request::get('expires'));
             $access_token = new \Sportily\OAuth\Token(Request::get('access_token'), $expiry, 'authorization_code', null);


             Session::put('access_token', $access_token);
         }
         return Session::get('access_token', Token::invalid());
     }

    /**
     * Write an access token back into the session.
     */
    private function putSessionToken($token) {
        Session::put('access_token', $token);
    }

}
