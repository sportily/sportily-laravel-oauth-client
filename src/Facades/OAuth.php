<?php
namespace Sportily\OAuth\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Laravel facade for the OAuthClient.
 */
class OAuth extends Facade {

    /**
     * Define the facade mapping.
     */
    protected static function getFacadeAccessor() {
        return 'sportily-oauth.client';
    }

}
