<?php
namespace Sportily\OAuth\Subscribers;

use Cache;
use GuzzleHttp\Event\BeforeEvent;
use GuzzleHttp\Event\ErrorEvent;
use GuzzleHttp\Event\RequestEvents;
use GuzzleHttp\Event\SubscriberInterface;
use Session;
use Sportily\OAuth\OAuthClient;

/**
 * A guzzle subscriber that adds the access token, present in the ,session into
 * each request.
 */
class OAuthSubscriber implements SubscriberInterface {

    // The OAuth client that handles requests to the OAuth service.
    private $oauth;

    /**
     * Construct a new middleware instance.
     */
    public function __construct(OAuthClient $oauth) {
        $this->oauth = $oauth;
    }

    /**
     * Define the set of captured events.
     */
    public function getEvents() {
        return [
            'before' => ['onBefore', RequestEvents::SIGN_REQUEST],
            'error' => ['onError']
        ];
    }

    /**
     * Before a request is sent (or signed), add in the Authorization header.
     */
    public function onBefore(BeforeEvent $event) {
        $token = $this->getAccessToken()->value;
        $event->getRequest()->setHeader('Authorization', 'Bearer ' . $token);
    }

    /**
     * Deal with 401 auth errors by requesting a new token, then replaying
     * the original request.
     */
    public function onError(ErrorEvent $event) {
        $req = $event->getRequest();
        $res = $event->getResponse();

        if ($res->getStatusCode() == 401) {
            $token = $this->oauth->getToken();
            $this->putSessionToken($token);
            $this->putCacheToken($token);
            $res = $event->getClient()->send($req);
            $event->intercept($res);
        }
    }

    /**
     * Pull the access token out of the session.
     */
    private function getAccessToken() {
        return Session::get('access_token');
    }

    /**
     * Write an access token back into the session.
     */
    private function putSessionToken($token) {
        Session::put('access_token', $token);
    }

    /**
     * Write an access token back into the cache.
     */
    private function putCacheToken($token) {
        Cache::forever('access_token', $token);
    }

}
