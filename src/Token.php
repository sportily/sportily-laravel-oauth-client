<?php
namespace Sportily\OAuth;

use Carbon\Carbon;

/**
 * An access token.
 */
class Token {

    // The actual token value.
    public $value;

    // The point at which the token expires.
    public $expiry;

    // The grant type used to generate the token.
    public $type;

    // The (optional) refresh token.
    public $refresh;

    /**
     * Construct a new token instance.
     */
    public function __construct($value, $expiry, $type, $refresh) {
        $this->value = $value;
        $this->expiry = $expiry;
        $this->type = $type;
        $this->refresh = $refresh;
    }

    /**
     * Return true if the token has a value and has not expired.
     */
    public function isValid() {
        return $this->value != null && !$this->isExpired();
    }

    /**
     * Return true if the token has expired.
     */
    public function isExpired() {
        return Carbon::now()->gt($this->expiry);
    }

    /**
     * Return true if the token is one that can be refreshed.
     */
    public function supportsRefresh() {
        return isset($this->refresh);
    }

    /**
     * Generate an invalid token instance.
     */
    public static function invalid() {
        return new Token(null, null, null, null);
    }

}
