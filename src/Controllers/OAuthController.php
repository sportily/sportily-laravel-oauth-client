<?php
namespace Sportily\OAuth\Controllers;

use GuzzleHttp\Exception\ClientException;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Sportily\OAuth\OAuthClient;

/**
 * Laravel controller to handle various OAuth flows.
 */
class OAuthController extends Controller {

    // The OAuth client that handles requests to the OAuth service.
    private $oauth;

    /**
     * Construct a new controller instance.
     */
    public function __construct(OAuthClient $oauth) {
        $this->oauth = $oauth;
    }

    /**
     * Handle callbacks from the OAuth server when the user has logged in.
     * Exchanges the auth code for an access token, then writes it into the
     * session for safe keeping.
     */
    public function callback(Request $request) {
        if ($request->has('code')) {
            $token = $this->oauth->getToken('authorization_code', [
                'code' => $request->get('code')
            ]);

            $request->session()->put('access_token', $token);
        }
        if ($request->get('return_to')) {
            return redirect($request->get('return_to'));
        }
        return redirect('/');
    }

    /**
     * Attempt to refresh the access token, using the refresh token.
     */
    public function refresh(Request $request) {
        $token = $request->session()->get('access_token');

        if ($token != null && $token->supportsRefresh()) {
            try {
                // attempt to refresh the token.
                $token = $this->oauth->refreshToken($token);
                $request->session()->put('access_token', $token);
                return [ 'access_token' => $token->value ];

            } catch (ClientException $e) {
                // unable to refresh the token, so back to login.
                return [ 'error' => 'refresh_failed' ];
            }
        }

        return [ 'error' => 'refresh_not_supported' ];
    }

    /**
     * Redirect the user to the OAuth logout flow, passing the current URL
     * along so the user can be redirected back to the current page.
     */
    public function logout(Request $request) {
        $request->session()->forget('access_token');
        return redirect($this->oauth->logoutUrl(url('/')));
    }
}
