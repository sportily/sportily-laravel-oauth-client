<?php
namespace Sportily\OAuth;

use Illuminate\Foundation\Application as LaravelApplication;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;

/**
 * A Laravel service provider that sets up routes to handle the OAuth authorize
 * and logout flows. In addition, provide an OAuthClient singleton that
 * encapsulates all the complexity of interacting with the OAuth server.
 */
class OAuthServiceProvider extends ServiceProvider {

    /**
     * Publish configuration and define callback/logout routes.
     */
    public function boot(Router $router) {
        $this->bootConfig($this->app);
        $this->bootRoutes($router);
    }

    /**
     * Publish configuration.
     */
    private function bootConfig($app) {
        $source = realpath(__DIR__ . '/../config/sportily-oauth.php');

        if ($app instanceof LaravelApplication && $app->runningInConsole()) {
            $this->publishes([$source => config_path('sportily-oauth.php')]);
        }

        $this->mergeConfigFrom($source, 'sportily-oauth');
    }

    /**
     * Define routes to handle the callback from the authorize flow, and to
     * handle logging out.
     */
    private function bootRoutes($router) {
        $config = [
            'middleware' => 'web',
            'namespace' => 'Sportily\OAuth\Controllers'
        ];

        $router->group($config, function($router) {
            $router->get('callback', 'OAuthController@callback');
            $router->get('refresh', 'OAuthController@refresh');
            $router->get('logout', 'OAuthController@logout');
        });
    }

    /**
     * Register an OAuthClient, used in the OAuth controllers and middleware.
     */
    public function register() {
        $this->app->singleton('sportily-oauth.client', function($app) {
            $config = $app['config']->get('sportily-oauth');
            return new GuzzleOAuthClient($config);
        });

        $this->app->alias('sportily-oauth.client', OAuthClient::class);
    }

}
