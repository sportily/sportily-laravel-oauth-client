# Sportily OAuth 2.0 Client for Laravel

An [OAuth 2.0](http://tools.ietf.org/wg/oauth/draft-ietf-oauth-v2/) client library for the Laravel framework, to integration your app with the [Sportily League Management](http://sporti.ly) service.

## Installation

Composer is the recommended way to install this package. Add the following line to your `composer.json` file:

```json
"sportily/sportily-laravel-oauth-client": "dev-master@dev"
```

Then run `composer update` to get the package.

Once composer has installed the package this the following line to the `providers` array, located in your `config/app.php` file:

```php
Sportily\OAuth\OAuthServiceProvider::class,
```

Add this line to the `aliases` array:

```php
'OAuth' => Sportily\OAuth\Facades\OAuth::class,
```

Add the following lines to the `$routeMiddleware` array, located in your `app/Http/Kernel.php` file:

```php
'auth.sportily.private' => \Sportily\OAuth\Middleware\OAuthMiddlewarePrivate::class,
'auth.sportily.public' => \Sportily\OAuth\Middleware\OAuthMiddlewarePublic::class,
```

Next, run `php artisan vendor:publish` to publish this package's configuration. You should not need to edit the configuration file directly. Instead, add the following lines to your `.env` file:

```
SPORTILY_OAUTH_REDIRECT_URL=http://<your-domain>/callback
SPORTILY_OAUTH_CLIENT_ID=<your-public-key>
SPORTILY_OAUTH_CLIENT_SECRET=<your-secret-key>
```

## Middleware

This package provides middleware that takes care of the complexity of acquiring and refreshing access tokens. Access tokens are required to make calls to the Sportily API service.

### Accessing public data

Much of the data exposed by the API is public, such as fixtures, venues, league statistics, and much more.

If all you want to do is retrieve public data from the API, it is sufficient decorate your routes with the `auth.sportily.public` middleware.

This middleware ensures that a valid access token is always present in the session.

### Accessing private data

If you require access to private data, you must ask the user to first login, granting access to everything that user has access to.

In such a scenario you should decorate your routes with the `auth.sportily.private` middleware.

This middleware will redirect the user to a login prompt, if they are not already logged in. If the user logs in and grants your application access, an access token will be added to the session that grants your application access to all of the user's private data.

## Next Steps

Use the [Sportily API client library](https://bitbucket.org/sportily/sportily-laravel-api-client) to make requests to the API.
