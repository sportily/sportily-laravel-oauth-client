<?php

return [
    'base_url'      => env('SPORTILY_OAUTH_BASE_URL', 'http://sporti.ly'),
    'redirect_url'  => env('SPORTILY_OAUTH_REDIRECT_URL'),
    'client_id'     => env('SPORTILY_OAUTH_CLIENT_ID'),
    'client_secret' => env('SPORTILY_OAUTH_CLIENT_SECRET'),
];
